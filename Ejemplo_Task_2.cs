using System;
using System.Threading;
using System.Threading.Tasks;

public class Ejemplo_Task_2
{

   //Jhustin Ismael Arias Perez 4-A T/M

   public static void Main()
   {
      
      Task[] tasks = new Task[100];
      for (int i = 0; i < 100; i++)
      {

          //Correr Task en 2 Segundos
          tasks[i] = Task.Run(() => Thread.Sleep(2000));
      }
      try {
         Task.WaitAll(tasks);
      }
      catch (AggregateException ae) {
         Console.WriteLine("Ocurrio una excepcion");
         foreach (var ex in ae.Flatten().InnerExceptions)
            Console.WriteLine("   {0}", ex.Message);
      }   

      Console.WriteLine("Task listos:");
      foreach (var t in tasks)
         Console.WriteLine("   Task #{0}: {1}", t.Id, t.Status);
   }
}