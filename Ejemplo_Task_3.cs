using System;
using System.Threading;
using System.Threading.Tasks;

public class Ejemplo_Task_3
{
   //Jhustin Ismael Arias Perez 4-A T/M

   public static void Main()
   {
      .
      var source1 = new CancellationTokenSource();
      var token1 = source1.Token;
      source1.Cancel();
      
      var source2 = new CancellationTokenSource();
      var token2 = source2.Token;
       
      Task[] tasks = new Task[12];
      for (int i = 0; i < 12; i++)
      {
          switch (i % 4) 
          {
             
             case 0:
                tasks[i] = Task.Run(() => Thread.Sleep(2000));
                break;
   
                tasks[i] = Task.Run( () => Thread.Sleep(2000),
                         token1);
                break;         
             case 2:
                
                tasks[i] = Task.Run( () => { throw new NotSupportedException(); } );
                break;
             case 3:
             
                tasks[i] = Task.Run( () => { Thread.Sleep(2000); 
                                             if (token2.IsCancellationRequested)
                                                token2.ThrowIfCancellationRequested();
                                             Thread.Sleep(500); }, token2);   
                break;
          }
      }
      Thread.Sleep(250);
      source2.Cancel();
       
      try {
         Task.WaitAll(tasks);
      }
      catch (AggregateException ae) {
          Console.WriteLine("One or more exceptions occurred:");
          foreach (var ex in ae.InnerExceptions)
             Console.WriteLine("   {0}: {1}", ex.GetType().Name, ex.Message);
       }   

      Console.WriteLine("\nStatus of tasks:");
      foreach (var t in tasks) {
         Console.WriteLine("   Task #{0}: {1}", t.Id, t.Status);
         if (t.Exception != null) {
            foreach (var ex in t.Exception.InnerExceptions)
               Console.WriteLine("      {0}: {1}", ex.GetType().Name,
                                 ex.Message);
         }
      }
   }
}